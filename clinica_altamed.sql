
CREATE SEQUENCE public.log_cd_log_seq;

CREATE TABLE public.Log (
                cd_log INTEGER NOT NULL DEFAULT nextval('public.log_cd_log_seq'),
                de_log VARCHAR NOT NULL,
                dt_log DATE NOT NULL,
                hr_log TIME NOT NULL,
                CONSTRAINT cd_log_pk PRIMARY KEY (cd_log)
);


ALTER SEQUENCE public.log_cd_log_seq OWNED BY public.Log.cd_log;

CREATE SEQUENCE public.quarto_cd_quarto_seq;

CREATE TABLE public.Quarto (
                cd_quarto INTEGER NOT NULL DEFAULT nextval('public.quarto_cd_quarto_seq'),
                nr_andar INTEGER NOT NULL,
                nr_capacidade INTEGER NOT NULL,
                CONSTRAINT cd_quarto_pk PRIMARY KEY (cd_quarto)
);


ALTER SEQUENCE public.quarto_cd_quarto_seq OWNED BY public.Quarto.cd_quarto;

CREATE SEQUENCE public.especialidade_cd_especialidade_seq;

CREATE TABLE public.Especialidade (
                cd_especialidade INTEGER NOT NULL DEFAULT nextval('public.especialidade_cd_especialidade_seq'),
                nm_especialidade VARCHAR NOT NULL,
                de_especialidade VARCHAR NOT NULL,
                CONSTRAINT cd_especialidade PRIMARY KEY (cd_especialidade)
);


ALTER SEQUENCE public.especialidade_cd_especialidade_seq OWNED BY public.Especialidade.cd_especialidade;

CREATE SEQUENCE public.endereco_cd_endereco_seq_1;

CREATE TABLE public.Endereco (
                cd_endereco INTEGER NOT NULL DEFAULT nextval('public.endereco_cd_endereco_seq_1'),
                nm_rua VARCHAR NOT NULL,
                nm_bairro VARCHAR NOT NULL,
                nm_cidade VARCHAR NOT NULL,
                nm_uf VARCHAR NOT NULL,
                cd_cep VARCHAR NOT NULL,
                CONSTRAINT cd_endereco_pk PRIMARY KEY (cd_endereco)
);


ALTER SEQUENCE public.endereco_cd_endereco_seq_1 OWNED BY public.Endereco.cd_endereco;

CREATE SEQUENCE public.pessoa_cd_pessoa_seq;

CREATE TABLE public.Pessoa (
                cd_pessoa INTEGER NOT NULL DEFAULT nextval('public.pessoa_cd_pessoa_seq'),
                nm_pessoa VARCHAR NOT NULL,
                dt_nascimento DATE NOT NULL,
                nr_rg VARCHAR NOT NULL,
                nr_cpf VARCHAR NOT NULL,
                fg_genero BOOLEAN NOT NULL,
                nr_telefone VARCHAR NOT NULL,
                cd_endereco INTEGER NOT NULL,
                CONSTRAINT cd_pessoa_pk PRIMARY KEY (cd_pessoa)
);


ALTER SEQUENCE public.pessoa_cd_pessoa_seq OWNED BY public.Pessoa.cd_pessoa;

CREATE SEQUENCE public.medico_cd_medico_seq;

CREATE TABLE public.Medico (
                cd_medico INTEGER NOT NULL DEFAULT nextval('public.medico_cd_medico_seq'),
                cd_pessoa INTEGER NOT NULL,
                nr_crm VARCHAR NOT NULL,
                fg_ativo BOOLEAN NOT NULL,
                vl_salario DOUBLE PRECISION NOT NULL,
                CONSTRAINT cd_medico PRIMARY KEY (cd_medico)
);


ALTER SEQUENCE public.medico_cd_medico_seq OWNED BY public.Medico.cd_medico;

CREATE TABLE public.Formacao (
                cd_medico INTEGER NOT NULL,
                cd_especialidade INTEGER NOT NULL,
                fg_exercicio BOOLEAN NOT NULL,
                CONSTRAINT cd_formacao_pk PRIMARY KEY (cd_medico, cd_especialidade)
);


CREATE SEQUENCE public.paciente_cd_paciente_seq;

CREATE TABLE public.Paciente (
                cd_paciente INTEGER NOT NULL DEFAULT nextval('public.paciente_cd_paciente_seq'),
                de_diagnostico VARCHAR NOT NULL,
                cd_pessoa INTEGER NOT NULL,
                gr_saude INTEGER NOT NULL,
                cd_medico_responsavel INTEGER NOT NULL,
                dt_entrada_espera DATE NOT NULL,
                hr_entrada_espera TIME NOT NULL,
                fg_atendido BOOLEAN NOT NULL,
                CONSTRAINT cd_paciente_pk PRIMARY KEY (cd_paciente)
);
COMMENT ON COLUMN public.Paciente.hr_entrada_espera IS 'ESTE CAMPO SERVA PARA CALCULAR O TEMPO DE QUANTIDADE DE DIAS QUE UM PACIENTE ESTÁ NA FILA DE ESPERA DA CLINICA.';


ALTER SEQUENCE public.paciente_cd_paciente_seq OWNED BY public.Paciente.cd_paciente;

CREATE TABLE public.Estadia (
                cd_quarto INTEGER NOT NULL,
                cd_paciente INTEGER NOT NULL,
                fg_infeccao BOOLEAN NOT NULL,
                dt_infeccao DATE,
                de_sintomas VARCHAR NOT NULL,
                CONSTRAINT cd_estadia_pk PRIMARY KEY (cd_quarto, cd_paciente)
);


CREATE SEQUENCE public.acompanhamento_cd_acompanhamento_seq;

CREATE TABLE public.Acompanhamento (
                cd_acompanhamento INTEGER NOT NULL DEFAULT nextval('public.acompanhamento_cd_acompanhamento_seq'),
                dt_visita DATE NOT NULL,
                nr_hora TIME NOT NULL,
                de_estado VARCHAR NOT NULL,
                vl_pressao VARCHAR NOT NULL,
                vl_temperatura INTEGER NOT NULL,
                cd_medico_visita INTEGER NOT NULL,
                cd_paciente INTEGER NOT NULL,
                CONSTRAINT cd_acompanhamento_pk PRIMARY KEY (cd_acompanhamento)
);


ALTER SEQUENCE public.acompanhamento_cd_acompanhamento_seq OWNED BY public.Acompanhamento.cd_acompanhamento;

ALTER TABLE public.Estadia ADD CONSTRAINT quarto_estadia_fk
FOREIGN KEY (cd_quarto)
REFERENCES public.Quarto (cd_quarto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Formacao ADD CONSTRAINT especialidade_formacao_fk
FOREIGN KEY (cd_especialidade)
REFERENCES public.Especialidade (cd_especialidade)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Pessoa ADD CONSTRAINT endereco_pessoa_fk
FOREIGN KEY (cd_endereco)
REFERENCES public.Endereco (cd_endereco)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Paciente ADD CONSTRAINT pessoa_paciente_fk
FOREIGN KEY (cd_pessoa)
REFERENCES public.Pessoa (cd_pessoa)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Medico ADD CONSTRAINT pessoa_medico_fk
FOREIGN KEY (cd_pessoa)
REFERENCES public.Pessoa (cd_pessoa)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Paciente ADD CONSTRAINT medico_paciente_fk
FOREIGN KEY (cd_medico_responsavel)
REFERENCES public.Medico (cd_medico)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Formacao ADD CONSTRAINT medico_formacao_fk
FOREIGN KEY (cd_medico)
REFERENCES public.Medico (cd_medico)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Acompanhamento ADD CONSTRAINT medico_acompanhamento_fk
FOREIGN KEY (cd_medico_visita)
REFERENCES public.Medico (cd_medico)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Acompanhamento ADD CONSTRAINT paciente_acompanhamento_fk
FOREIGN KEY (cd_paciente)
REFERENCES public.Paciente (cd_paciente)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Estadia ADD CONSTRAINT paciente_estadia_fk
FOREIGN KEY (cd_paciente)
REFERENCES public.Paciente (cd_paciente)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
