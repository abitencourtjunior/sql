CREATE OR REPLACE FUNCTION ordenar_paciente_func()
RETURNS SETOF paciente as $$ 
BEGIN
 RETURN query( SELECT * FROM paciente ORDER BY cd_gravidade);
END;
$$ LANGUAGE plpgsql;

-- Execução 

SELECT * FROM ordenar_paciente_func();

-- Paleativa

CREATE OR REPLACE VIEW vw_pacientes_gr AS 
        SELECT cd_paciente, gr_saude, de_diagnostico, nm_pessoa, cd_medico_responsavel, dt_entrada_espera,
        fg_atendido from paciente inner join pessoa on paciente.cd_pessoa = pessoa.cd_pessoa order by gr_saude DESC;

-- Não é procedimento, é uma regra; 
-- Resolve, mas não é procedimento; 

CREATE OR REPLACE RULE ordenar_pacientes AS ON SELECT TO vw_pacientes_gr 
        DO INSTEAD SELECT cd_paciente, gr_saude, de_diagnostico, nm_pessoa, cd_medico_responsavel, dt_entrada_espera, fg_atendido 
        from paciente inner join pessoa on paciente.cd_pessoa = pessoa.cd_pessoa order by gr_saude DESC; 

-- Teste para Verificar a Ordenação;
INSERT INTO endereco (nm_rua, nm_bairro, nm_cidade, nm_uf, cd_cep) values ('Rua amor', 'Do Zé', 'Altacity', 'ALTAC', 91230111);
INSERT INTO pessoa (nm_pessoa, dt_nascimento, nr_rg, nr_cpf, fg_genero, nr_telefone, cd_endereco) VALUES ('Altamir', '2018-04-10', '123', '123', true,'(32)2222-2222', 1);
insert into paciente (de_diagnostico, cd_pessoa, gr_saude, cd_medico_responsavel, dt_entrada_espera, fg_atendido) VALUES ('Hernia de Disco', 2, 3,4,'2018-10-28', current_time ,true);
insert into medico (cd_pessoa, nr_crm, fg_ativo, vl_salario) values (1, 12877897867, true, 14.4540);
update fg_atendido=true




