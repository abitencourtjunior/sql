CREATE OR REPLACE FUNCTION func_tempo_espera()
RETURNS SETOF paciente AS $$
BEGIN
   RETURN QUERY( select  (current_time - hr_entrada_espera), (current_date - dt_entrada_espera) from "Paciente");
END;

$$ LANGUAGE plpgsql;



-- Select para verificar o tempo e dia de cada linha com o de agora para cada linha do log:  
-- select  (current_time - hr_entrada_espera), (current_date - dt_entrada_espera) from paciente;


