CREATE OR REPLACE FUNCTION medico_log_func()                                                   
RETURNS Trigger as $log_registros$
BEGIN 
    IF(TG_OP = 'INSERT') THEN
        -- BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES  
        ('[Médico] Operação de inserção. A linha de código '|| NEW.cd_medico || ' foi inserido com os dados: ' || NEW.* ||'.', current_date, localtime );
        RETURN NEW;

    ELSIF(TG_OP = 'UPDATE') THEN 
        -- BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        ('[Médico] Operação de UPDATE. A linha de código ' || NEW.cd_medico || ' teve os valores atualizados ' || OLD || ' com ' || NEW.* || '.', current_date, localtime);
        RETURN NEW;
    
    ELSIF(TG_OP = 'DELETE') THEN
        --  BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        (' [Médico] Operação DELETE. A linha de código ' || OLD.* || ' foi excluída. ', current_date, localtime);
        RETURN OLD;
    END IF;
    RETURN NULL;
END; 
$log_registros$ LANGUAGE plpgsql;

CREATE trigger log_registros                                                   
AFTER INSERT OR UPDATE OR DELETE ON medico
    for each row 
        execute procedure medico_log_func();


insert into medico (cd_pessoa, nr_crm, fg_ativo, vl_salario) values (1, 12877897867, true, 14.4540);
