CREATE OR REPLACE FUNCTION paciente_log_func()                                                         
RETURNS Trigger as $log_registros$
BEGIN 
    IF(TG_OP = 'INSERT') THEN
        -- BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES  
        ('[Paciente] Operação de Inserção. O ID paciente '|| NEW.cd_paciente || ' com a ID_PESSOA: '|| NEW.* || '.', current_date, localtime );
        RETURN NEW;

    ELSIF(TG_OP = 'UPDATE') THEN 
        -- BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        ('[Paciente] Operação de UPDATE. A linha de código ' || NEW.cd_paciente || ' teve os valores atualizados ' || OLD || ' com ' || NEW.* || '.', current_date, localtime);
        RETURN NEW;
    
    ELSIF(TG_OP = 'DELETE') THEN
        --  BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        ('[Paciente] Operação DELETE. A linha de código ' || OLD.* || ' foi excluída. ', current_date, localtime);
        RETURN OLD;
    END IF;
    RETURN NULL;
END; 
$log_registros$ LANGUAGE plpgsql;

CREATE trigger log_registros                             
AFTER INSERT OR UPDATE OR DELETE ON paciente
    for each row 
        execute procedure paciente_log_func();

-- Teste de Insert 

insert into paciente (de_diagnostico, cd_pessoa, gr_saude, cd_medico_responsavel, dt_entrada_espera, fg_atendido) VALUES ('Hernia de Disco', 2, 1,4,'2018-10-28',true);
