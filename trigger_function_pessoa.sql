CREATE OR REPLACE FUNCTION pessoa_log_func()                                                                     
RETURNS trigger as $log_registro$
BEGIN 
    IF(TG_OP = 'INSERT') THEN
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        ( '[Pessoa] Operação de inserção. A linha de código ' || NEW.cd_pessoa || ' foi inserido com os dados: ' || NEW.* ||'.', current_date, localtime);
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE')THEN
        INSERT INTO log (de_log, dt_log, hr_log) VALUES
        ('[Pessoa] Operação de UPDATE. A linha de código ' || NEW.cd_pessoa || ' teve os valores atualizados ' || OLD || ' com ' || NEW.* || '.', current_date, localtime);
        RETURN NEW;
    
    ELSIF (TG_OP = 'DELETE') THEN 
        INSERT INTO log (de_log, dt_log, hr_log) VALUES
        ('[Pessoa] Operação DELETE. A linha de código ' || OLD.* || ' foi excluída ', current_date, localtime);
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$log_registro$ LANGUAGE plpgsql;

CREATE trigger log_registros                                                   
AFTER INSERT OR UPDATE OR DELETE ON pessoa
    for each row 
        execute procedure pessoa_log_func();
