CREATE OR REPLACE FUNCTION paciente_atribuir_medico_log_func()                                                         
RETURNS Trigger as $log_registros$
BEGIN 

    IF(TG_OP = 'UPDATE') THEN 
        -- BLOCO DE AÇÃO
        INSERT INTO log (de_log, dt_log, hr_log) VALUES 
        ('[ Atribuindo Médico - Paciente ] Operação de UPDATE em paciente. O paciente: ' || NEW.cd_paciente || ' foi atribuido para outro médico, com o ID Médico - [Antigo] : ' || OLD.cd_medico_responsavel || ' para o novo médico ID Médico - [Atual] ' || NEW.cd_medico_responsavel || '.', current_date, localtime);
    END IF;
    RETURN NULL;
END; 
$log_registros$ LANGUAGE plpgsql;

CREATE TRIGGER tg_paciente_atribuir_medico
    BEFORE UPDATE ON paciente
    FOR EACH ROW
        WHEN (OLD.cd_medico_responsavel IS DISTINCT FROM NEW.cd_medico_responsavel)
    EXECUTE PROCEDURE paciente_atribuir_medico_log_func();

-- Teste de Script - Trigger - Atribuindo novo médico: UPDATE paciente SET cd_medico_responsavel = 6 WHERE cd_paciente = 3;
