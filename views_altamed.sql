CREATE VIEW vw_Acompanhamento AS SELECT * FROM acompanhamento;
CREATE VIEW vw_Endereco AS SELECT * FROM endereco;
CREATE VIEW vw_Especialidade AS SELECT * FROM especialidade;
CREATE VIEW vw_Estadia AS SELECT * FROM estadia;
CREATE VIEW vw_Formacao AS SELECT * FROM formacao;
CREATE VIEW vw_Log AS SELECT * FROM log;
CREATE VIEW vw_Medico AS SELECT * FROM medico;
CREATE VIEW vw_Paciente AS SELECT * FROM paciente;
CREATE VIEW vw_Pessoa AS SELECT * FROM pessoa;
CREATE VIEW vw_Quarto AS SELECT * FROM quarto;
CREATE VIEW vw_Usuario AS SELECT * FROM usuario;

-- View Especiais 

CREATE VIEW vw_pessoa_completo AS 
    SELECT * FROM "Pessoa" AS p 
        JOIN "Endereco" AS e 
            ON p.endereco_cd = e.cd_endereco;

CREATE VIEW vw_medico_completo AS 
    SELECT m.cd_medico, m.cd_pessoa, m.nr_crm, m.fg_ativo, m.vl_salario , e.cd_especialidade, f.fg_exercicio , e.nm_especialidade
    FROM medico AS m 
        JOIN formacao AS f 
            ON m.cd_medico = f.cd_medico 
        JOIN especialidade AS e
            ON f.cd_especialidade = e.cd_especialidade;

CREATE VIEW VW_paciente_completo AS 
	SELECT * FROM "Paciente" AS p JOIN "Acompanhamento" AS a ON p.cd_paciente = a.cd_paciente

CREATE VIEW vw_pacientes_ordenados AS
SELECT paciente.cd_paciente,pessoa.nm_pessoa, paciente.de_diagnostico, medico.nr_crm, paciente.dt_entrada_espera,paciente.fg_atendido,paciente.cd_gravidade FROM vw_Pessoa AS pessoa JOIN vw_Paciente AS paciente ON pessoa.cd_pessoa = paciente.cd_pessoa JOIN vw_Medico AS medico ON paciente.cd_medico_responsavel = medico.cd_medico ORDER BY cd_gravidade;





            

